import React, { Component } from 'react';

import {
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
  } from 'react-native';

import {
    ViroARSceneNavigator
  } from 'react-viro';

import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';

/*
 TODO: Insert your API key below
 */
var sharedProps = {
    apiKey:"API_KEY_HERE",
}

// Sets the default scene you want for AR and VR
var InitialARScene = require('./SceneAR.js');
var AR_NAVIGATOR_TYPE = "AR";
var UNSET = "UNSET";

export default class ModelConfiguration extends Component {

    constructor() {
        super();
        
        this.state = {
            sharedProps : sharedProps
        }
        
        this._getARNavigator = this._getARNavigator.bind(this);
        this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(this);
        this._exitViro = this._exitViro.bind(this);
    }

    _goToAbout = () => {
        Actions.about()
    }
    
    // Returns the ViroARSceneNavigator which will start the AR experience
    _getARNavigator() {
        return (
        <ViroARSceneNavigator {...this.state.sharedProps}
            initialScene={{scene: InitialARScene}} />
        );
    }


    // This function returns an anonymous/lambda function to be used
    // by the experience selector buttons
    _getExperienceButtonOnPress(navigatorType) {
        return () => {
        this.setState({
            navigatorType : navigatorType
        })
        }
    }

    // This function "exits" Viro by setting the navigatorType to UNSET.
    _exitViro() {
        this.setState({
        navigatorType : UNSET
        })
    }

    render() { 
        /*            <TouchableOpacity style = {{ margin: 128 }} onPress = {goToAbout}>
                <Text>This is HOME!</Text>
            </TouchableOpacity>*/
        return (
          <ViroARSceneNavigator {...this.state.sharedProps}
              initialScene={{scene: InitialARScene}} />
        )
    }
}
var localStyles = StyleSheet.create({
    viroContainer :{
      flex : 1,
      backgroundColor: "black",
    },
    outer : {
      flex : 1,
      flexDirection: 'row',
      alignItems:'center',
      backgroundColor: "black",
    },
    inner: {
      flex : 1,
      flexDirection: 'column',
      alignItems:'center',
      backgroundColor: "black",
    },
    titleText: {
      paddingTop: 30,
      paddingBottom: 20,
      color:'#fff',
      textAlign:'center',
      fontSize : 25
    },
    buttonText: {
      color:'#fff',
      textAlign:'center',
      fontSize : 20
    },
    buttons : {
      height: 80,
      width: 150,
      paddingTop:20,
      paddingBottom:20,
      marginTop: 10,
      marginBottom: 10,
      backgroundColor:'#68a0cf',
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#fff',
    },
    exitButton : {
      height: 50,
      width: 100,
      paddingTop:10,
      paddingBottom:10,
      marginTop: 10,
      marginBottom: 10,
      backgroundColor:'#68a0cf',
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#fff',
    }
});
module.exports = ModelConfiguration;