'use strict';

import React, { Component } from 'react';

import { StyleSheet } from 'react-native';

import {
  ViroARScene,
  ViroText,
  ViroConstants,
  ViroBox,
  ViroMaterials,
  Viro3DObject,
  ViroAmbientLight,
  ViroSpotLight,
  ViroARPlaneSelector,
  ViroNode,
  ViroAnimations,
  ViroDirectionalLight,
} from 'react-viro';

export default class HelloWorldSceneAR extends Component {

  constructor() {
    super();

    // Set initial state here
    this.state = {
      text: "Initializing AR..."
    };

    // bind 'this' to functions
    this._onInitialized = this._onInitialized.bind(this);
  }

  render() {
    /*
            <ViroText text={this.state.text} scale={[.5, .5, .5]} position={[0, 0, -1]} style={styles.helloWorldTextStyle} />
        <ViroBox position={[0, -.5, -1]} scale={[.3, .3, .1]} materials={["grid"]} animation={{name: "rotate", run: true, loop: true}}/>
            <ViroNode position={[0,-1,0]} dragType="FixedToWorld" onDrag={()=>{}} >
          <Viro3DObject
              source={require('./res/emoji_smile/emoji_smile.vrx')}
              resources={[require('./res/emoji_smile/emoji_smile_diffuse.png'),
                  require('./res/emoji_smile/emoji_smile_normal.png'),
                  require('./res/emoji_smile/emoji_smile_specular.png')]}
              position={[0, .5, -1]}
              scale={[.2, .2, .2]}
              type="VRX" />
        </ViroNode>
    */
    return (
      <ViroARScene onTrackingUpdated={this._onInitialized} >
        <ViroAmbientLight color={"#FFFFFF"} />
        <ViroDirectionalLight
          color="#FFFFFF"
          direction={[0, -1, 0]}
        />
        <ViroDirectionalLight
          color="#FFFFFF"
          direction={[0, -1, 0]}
        />
        <Viro3DObject
          source={require('./res/CARROZZERIA.glb')}
          position={[0, 0, -1]}
          scale={[0.02, 0.02, 0.02]}
          type="GLB" />
        <Viro3DObject
          source={require('./res/CALIPERS.glb')}
          position={[0, 0, -1]}
          scale={[0.02, 0.02, 0.02]}
          type="GLB" />
        <Viro3DObject
          source={require('./res/WHEELS.glb')}
          position={[0, 0, -1]}
          scale={[0.02, 0.02, 0.02]}
          type="GLB" />
        <Viro3DObject
          source={require('./res/INTERNI + CARBONIO.glb')}
          position={[0, 0, -1]}
          scale={[0.02, 0.02, 0.02]}
          type="GLB" />
      </ViroARScene>
    );
  }

  _onInitialized(state, reason) {
    if (state == ViroConstants.TRACKING_NORMAL) {
      this.setState({
        text: "ANESIS!"
      });
    } else if (state == ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }
}

var styles = StyleSheet.create({
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});

ViroMaterials.createMaterials({
  grid: {
    diffuseTexture: require('./res/grid_bg.jpg'),
  },
});

ViroAnimations.registerAnimations({
  rotate: {
    properties: {
      rotateY: "+=90"
    },
    duration: 250, //.25 seconds
  },
});

module.exports = HelloWorldSceneAR;
